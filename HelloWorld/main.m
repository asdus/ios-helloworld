//
//  main.m
//  HelloWorld
//
//  Created by Ivan Ovcherenko on 03.10.13.
//  Copyright (c) 2013 Taburetka inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TBRTKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TBRTKAppDelegate class]));
    }
}
