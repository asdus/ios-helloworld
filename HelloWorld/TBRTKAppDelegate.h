//
//  TBRTKAppDelegate.h
//  HelloWorld
//
//  Created by Ivan Ovcherenko on 03.10.13.
//  Copyright (c) 2013 Taburetka inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TBRTKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
